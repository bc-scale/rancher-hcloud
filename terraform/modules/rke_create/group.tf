# Provides a Hetzner Cloud Placement Group to represent a Placement Group in the Hetzner Cloud:
# https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/placement_group

resource "hcloud_placement_group" "rancher" {
  name = "rancher-management-nodes"

  type = "spread"

  labels = {
    "project-name" = var.project_name,
    "builder"      = "terraform",
  }
}
