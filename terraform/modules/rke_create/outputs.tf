output "server_address" {
  description = "Public IPv4 addresses of all HCloud servers."

  value = [
    hcloud_server.rancher.*.ipv4_address,
  ]
}

output "server_private_address" {
  description = "Private IPv4 addresses of all HCloud servers."

  value = [
    hcloud_server_network.rancher.*.ip
  ]
}

output "lb_address" {
  description = "Public IPv4 address of the HCloud Load Balancer."

  value = hcloud_load_balancer.rancher.ipv4
}

output "kubeconfig_path" {
  description = "Credentials file to access the RKE being used as Rancher Management Cluster."

  value = local_file.kube_config_server_yaml.filename
}

output "kubeconfig_yaml" {
  description = "Credentials file to access the RKE being used as Rancher Management Cluster."

  value = rke_cluster.rancher.kube_config_yaml
}
