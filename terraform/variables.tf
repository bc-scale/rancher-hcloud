variable "hcloud_token" {
  type        = string
  description = "The token that is used to interact with the Hetzner Cloud API."
}

variable "dns_token" {
  type        = string
  description = "The token that is used to interact with the Hetzner DNS Console API."
}

variable "hcloud_ssh_key_path" {
  type        = string
  default     = "~/.ssh/rancher"
  description = "Path to the private key you want to use register on your Hetzner Cloud machines. The public key must have the same location and a .pub ending."
}

variable "letsencrypt_issuer" {
  type        = string
  default     = null
  description = "Mail address that is used to create the Let's Encrypt issuer. You will get notifications if your certs are expiring."
}

variable "rancher_admin_password" {
  type        = string
  default     = null
  description = "Password that is used for your Rancher admin login when the Rancher installation gets bootstrapped."
}

variable "project_domain" {
  type        = string
  default     = null
  description = "You need to enable HCloud DNS Console for this domain."
}

variable "instance_prefix" {
  type        = string
  default     = "manager-node"
  description = "The prefix that comes before the index-value to form the name of the machine."
}

variable "instance_count" {
  type        = number
  default     = 3
  description = "Number of instances that will be deployed. Should be a odd number (1, 3, 5, etc.)."
}

variable "instance_type" {
  type        = string
  default     = "cx31"
  description = "Hetzner instance type that is used for the machines. You can use the Hetzner Cloud CLI or browse their website to get a list of valid instance types."
}

variable "instance_zones" {
  type        = list(string)
  default     = ["nbg1", "fsn1", "hel1"]
  description = "All zones over which the nodes are distributed."
}

variable "lb_name" {
  type        = string
  default     = "rancher-manage-lb"
  description = "Name of the Load Balancer that is placed in front of your instances."
}

variable "lb_type" {
  type        = string
  default     = "lb11"
  description = "Hetzner Load Balancer type. You can use the Hetzner Cloud CLI or browse their website to get a list of valid instance types."
}

variable "lb_location" {
  type        = string
  default     = "nbg1"
  description = "Location of the Load Balancer."
}

variable "network_name" {
  type        = string
  default     = "rancher"
  description = "Name of the private network that is created for the nodes."
}

variable "network_ip_range" {
  type        = string
  default     = "172.16.0.0/12"
  description = "IP address range of the private network that is created for the nodes."
}

variable "subnet_nodes_ip_range" {
  type        = string
  default     = "172.16.1.0/24"
  description = "IP address range of the nodes within the private network being created."
}

variable "network_zone" {
  type        = string
  default     = "eu-central"
  description = "Zone of the private network that is created for the nodes."
}

variable "project_name" {
  type        = string
  default     = "rancher-hcloud-meetup"
  description = "Name of the project this setup is provisioned for."
}
